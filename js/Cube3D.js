/* For EACH POLYFILL */
// Production steps of ECMA-262, Edition 5, 15.4.4.18
// Reference: http://es5.github.io/#x15.4.4.18
if (!Array.prototype.forEach) {
  Array.prototype.forEach = function(callback, thisArg) {
    var T, k;
    if (this == null) {
      throw new TypeError(' this is null or not defined');
    }
    var O = Object(this);
    var len = O.length >>> 0;
    if (typeof callback !== "function") {
      throw new TypeError(callback + ' is not a function');
    }
    if (arguments.length > 1) {
      T = thisArg;
    }
    k = 0;
    while (k < len) {
      var kValue;
      if (k in O) {
        kValue = O[k];
        callback.call(T, kValue, k, O);
      }
      k++;
    }
  };
}
/* END OF FOREACH POLYFILL */


var Cube3D = function() {
  this.images = Array.prototype.slice.call(arguments),
  this.faceOn = 0
}

Cube3D.prototype.createCube = function(target) {
  var images = this.images;
  var faces = [];

  var container = document.createElement("div");
  container.className = "container";

  var cube = document.createElement("div");
  cube.className = "cube";

  images.forEach(function(image, index){
    var el = document.createElement("div");
    if(typeof image === "object") {
      if(image.video) {
        var vid = document.createElement("video");
        image.background ? el.style.backgroundImage = "url("+image.background+")" : "";
        image.controls ? vid.setAttribute("controls",  "controls") : "";
        image.autoplay ? vid.setAttribute("autoplay", "autoplay") : "";
        image.poster ? vid.setAttribute("poster", image.poster) : "";
        vid.style.width = "100%";
        vid.style.height = "auto";

        var source = document.createElement("source");
        source.src = image.video;
        source.setAttribute("type", image.type || "video/mp4");

        var togglePlay = function(){
          vid.paused ? vid.play() : vid.pause();
        };

        vid.addEventListener("click", togglePlay)
        vid.appendChild(source);
        el.appendChild(vid);
      }
    } else if (typeof image === "string") {
      el.style.backgroundImage = "url("+image+")";
    }

    index == 0 ? el.className += "face front" : el.className = "face back";
    faces.push(el);
    cube.appendChild(el);
  });

  container.appendChild(cube);
  target.appendChild(container);

  this.container = container;
  this.faces = faces;
  this.idle();
}

Cube3D.prototype.init = function(container) {
  container = container || document.body;
  this.createCube(container)
  this.on(this.container, "slide", this.moveCube);
}

Cube3D.prototype.onFaceOn = function() {
  return this;
};

Cube3D.prototype.idle = function(){
  this.faces[0].className = "face front idle-front";
  this.faces[1].className = "face back idle-back";
};

Cube3D.prototype.removeIdle = function(){
  this.faces[0].className = "face front";
  this.faces[1].className = "face back";
}

Cube3D.prototype.setFaceOn = function(){
  var that = this;
  return window.setTimeout(function(){
    that.removeTransition();
    if(that.faceOn < that.faces.length)that.faceOn++;
    return that.onFaceOn(that);
  }, 300);
}

Cube3D.prototype.removeTransition = function(){
  this.faces[this.faceOn].style.transition = "";
  this.faces[this.faceOn].style.WebkitTransition = "";
  this.faces[this.faceOn+1].style.transition = "";
  this.faces[this.faceOn+1].style.WebkitTransition = "";
}

Cube3D.prototype.moveCube = function(event, o){
    var faces = this.faces,
        width = parseInt(getStyle(this.container,"width")),
        height = parseInt(getStyle(this.container, "height")),
        that = this;

    this.removeIdle();
    if(this.faceOn < faces.length-1) {
      if(o.direction == "right" && (o.dx > -width && o.dx < width)) {
        if(!o.inMotion && o.dx != 0) {

          faces[this.faceOn].style.transition = "transform 0.5s";
          faces[this.faceOn].style.WebkitTransition = "-webkit-transform 0.5s";
          faces[this.faceOn+1].style.transition = "transform 0.5s";
          faces[this.faceOn+1].style.WebkitTransition = "-webkit-transform 0.5s";

          faces[this.faceOn].style.transform = "translateZ(0)scale(0,1)";
          faces[this.faceOn].style.WebkitTransform = "translateZ(0)scale(0,1)";

          faces[this.faceOn+1].style.transform = "translateZ(0)scale(1,1)";
          faces[this.faceOn+1].style.WebkitTransform = "translateZ(0)scale(1,1)";

          this.setFaceOn();

        } else {
          faces[this.faceOn].style.transformOrigin = "100% 50%";
          faces[this.faceOn].style.WebkitTransformOrigin = "100% 50%";
          faces[this.faceOn].style.transform = "translateZ("+(o.dx/width)*20+"vw)scale("+(1-((o.dx/width)*1))+",1)";
          faces[this.faceOn].style.WebkitTransform = "translateZ("+(o.dx/width)*20+"vw)scale("+(1-((o.dx/width)*1))+",1)";

          faces[this.faceOn+1].style.transformOrigin = "0% 50%";
          faces[this.faceOn+1].style.WebkitTransformOrigin = "0% 50%";
          faces[this.faceOn+1].style.transform = "translateZ("+(o.dx/width)*20+"vw)scale("+(o.dx/width)*1+",1)";
          faces[this.faceOn+1].style.WebkitTransform = "translateZ("+(o.dx/width)*20+"vw)scale("+(o.dx/width)*1+",1)";
        }

      } else if(o.direction == "left" && (o.dx > -width && o.dx < width)) {

        if(!o.inMotion && o.dx != 0) {
          faces[this.faceOn].style.transition = "-webkit-transform 0.5s";
          faces[this.faceOn].style.WebkitTransition = "transform 0.5s";
          faces[this.faceOn+1].style.transition = "transform 0.5s";
          faces[this.faceOn+1].style.WebkitTransition = "-webkit-transform 0.5s";

          faces[this.faceOn].style.transform = "translateZ(0)scale(0,1)";
          faces[this.faceOn].style.WebkitTransform = "translateZ(0)scale(0,1)";

          faces[this.faceOn+1].style.transform = "translateZ(0)scale(1,1)";
          faces[this.faceOn+1].style.WebkitTransform = "translateZ(0)scale(1,1)";

          this.setFaceOn();

        } else {
          faces[this.faceOn].style.WebkitTransformOrigin = "0% 50%";
          faces[this.faceOn].style.transformOrigin = "0% 50%";
          faces[this.faceOn].style.transform = "translateZ("+(-o.dx/width)*20+"vw)scale("+(1+((o.dx/width)*1))+",1)";
          faces[this.faceOn].style.WebkitTransform = "translateZ("+(-o.dx/width)*20+"vw)scale("+(1+((o.dx/width)*1))+",1)";

          faces[this.faceOn+1].style.transformOrigin = "100% 50%";
          faces[this.faceOn+1].style.WebkitTransformOrigin = "100% 50%";
          faces[this.faceOn+1].style.transform = "translateZ("+(-o.dx/width)*20+"vw)scale("+(-o.dx/width)*1+",1)";
          faces[this.faceOn+1].style.WebkitTransform = "translateZ("+(-o.dx/width)*20+"vw)scale("+(-o.dx/width)*1+",1)";
        }
      } else if(o.direction == "bottom" && (o.dy > -height && o.dy < height)) {
        if(!o.inMotion && o.dy != 0) {
          faces[this.faceOn].style.transition = "transform 0.5s";
          faces[this.faceOn].style.WebkitTransition = "-webkit-transform 0.5s";
          faces[this.faceOn+1].style.transition = "transform 0.5s";
          faces[this.faceOn+1].style.WebkitTransition = "-webkit-transform 0.5s";

          faces[this.faceOn].style.transform = "translateZ(0)scale(1,0)";
          faces[this.faceOn].style.WebkitTransform = "translateZ(0)scale(1,0)";

          faces[this.faceOn+1].style.transform = "translateZ(0)scale(1,1)";
          faces[this.faceOn+1].style.WebkitTransform = "translateZ(0)scale(1,1)";

          this.setFaceOn();

        } else {
          faces[this.faceOn].style.WebkitTransformOrigin = "50% 100%";
          faces[this.faceOn].style.transformOrigin = "50% 100%";
          faces[this.faceOn].style.transform = "translateZ("+(o.dy/height)*20+"vw)scale(1,"+(1-((o.dy/height)*1))+")";
          faces[this.faceOn].style.WebkitTransform = "translateZ("+(o.dy/height)*20+"vw)scale(1,"+(1-((o.dy/height)*1))+")";

          faces[this.faceOn+1].style.transformOrigin = "50% 0%";
          faces[this.faceOn+1].style.WebkitTransformOrigin = "50% 0%";
          faces[this.faceOn+1].style.transform = "translateZ("+(o.dy/height)*20+"vw)scale(1,"+(o.dy/height)*1+")";
          faces[this.faceOn+1].style.WebkitTransform = "translateZ("+(o.dy/height)*20+"vw)scale(1,"+(o.dy/height)*1+")";
        }
      } else if (o.direction == "top" && (o.dy > -height && o.dy < height)) {
        if(!o.inMotion && o.dy != 0) {
          faces[this.faceOn].style.transition = "transform 0.5s";
          faces[this.faceOn].style.WebkitTransition = "-webkit-transform 0.5s";
          faces[this.faceOn+1].style.transition = "transform 0.5s";
          faces[this.faceOn+1].style.WebkitTransition = "-webkit-transform 0.5s";

          faces[this.faceOn].style.transform = "translateZ(0)scale(1,0)";
          faces[this.faceOn].style.WebkitTransform = "translateZ(0)scale(1,0)";

          faces[this.faceOn+1].style.transform = "translateZ(0)scale(1,1)";
          faces[this.faceOn+1].style.WebkitTransform = "translateZ(0)scale(1,1)";

          this.setFaceOn();

        } else {
          faces[this.faceOn].style.transformOrigin = "50% 0%";
          faces[this.faceOn].style.WebkitTransformOrigin = "50% 0%";
          faces[this.faceOn].style.transform = "translateZ("+(-o.dy/height)*20+"vw)scale(1,"+(1+((o.dy/height)*1))+")";
          faces[this.faceOn].style.WebkitTransform = "translateZ("+(-o.dy/height)*20+"vw)scale(1,"+(1+((o.dy/height)*1))+")";

          faces[this.faceOn+1].style.WebkitTransformOrigin = "50% 100%";
          faces[this.faceOn+1].style.transformOrigin = "50% 100%";
          faces[this.faceOn+1].style.transform = "translateZ("+(-o.dy/height)*20+"vw)scale(1,"+(-o.dy/height)*1+")";
          faces[this.faceOn+1].style.WebkitTransform = "translateZ("+(-o.dy/height)*20+"vw)scale(1,"+(-o.dy/height)*1+")";
        }
      }
    }
}

Cube3D.prototype.on = function(element, evt, callback) {
var faces = this.faces;
var faceOn = this.faceOn;
var that = this;

var isSlide = evt == 'slide',
  detecttouch = !!('ontouchstart' in window) || !!('ontouchstart' in document.documentElement) || !!window.ontouchstart || !!window.onmsgesturechange || (window.DocumentTouch && window.document instanceof window.DocumentTouch);
  if (isSlide) {
    var o = {},
      evtStarted = false,
      evtStart = function(e) {
        var evt = e.changedTouches ? e.changedTouches[0] : e;
        evtStarted = true;
        o = {
          start: {
            left: evt.pageX,
            top: evt.pageY
          }
        };
      },
      evtEnd = function(e) {
        if (!evtStarted) return;
        var evt = e.changedTouches ? e.changedTouches[0] : e;
        o.end = {
          left: evt.pageX,
          top: evt.pageY
        };
        o.dx = o.end.left - o.start.left;
        o.dy = o.end.top - o.start.top;
        o.angle = Math.atan2(o.dy, o.dx);
        o.angle *= 180 / Math.PI;
        o.inMotion = (e.type == 'touchmove' || e.type == 'mousemove');
        o.direction = Math.abs(o.dx) > Math.abs(o.dy) ? ('' + o.dx).indexOf('-') != -1 ? 'left' : 'right' : ('' + o.dy).indexOf('-') != -1 ? 'top' : 'bottom',
          callback.apply(that, [e, o]);
        if (o.inMotion == false) evtStarted = false;
      };
    if (detecttouch) {
      element.addEventListener('touchstart', evtStart, false);
      element.addEventListener('touchmove', evtEnd, false);
      element.addEventListener('touchend', evtEnd, false);
    } else {
      element.addEventListener('mousedown', evtStart, false);
      element.addEventListener('mousemove', evtEnd, false);
      element.addEventListener('mouseup', evtEnd, false);
    }
  }
};

var getStyle = function(element, property) {
  return getComputedStyle(element, null).getPropertyValue(property);
}
