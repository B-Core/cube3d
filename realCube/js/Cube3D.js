var css = ".container{width:70vw;height:70vw;position:relative;margin:15vw auto 40px;-webkit-perspective:1000px;-moz-perspective:1000px;-o-perspective:1000px;perspective:1000px}#cube{width:100%;height:100%;position:absolute;-webkit-transform-style:preserve-3d;-moz-transform-style:preserve-3d;-o-transform-style:preserve-3d;transform-style:preserve-3d;-webkit-transition:-webkit-transform 1s;-moz-transition:-moz-transform 1s;-o-transition:-o-transform 1s;transition:transform 1s}#cube figure{display:block;position:absolute;width:100%;height:100%;border:2px solid black;line-height:196px;font-size:120px;font-weight:bold;color:white;text-align:center}#cube.panels-backface-invisible figure{-webkit-backface-visibility:hidden;-moz-backface-visibility:hidden;-o-backface-visibility:hidden;backface-visibility:hidden}#cube .front{background:hsla(0,100%,50%,0.7)}#cube .back{background:hsla(60,100%,50%,0.7)}#cube .right{background:hsla(120,100%,50%,0.7)}#cube .left{background:hsla(180,100%,50%,0.7)}#cube .top{background:hsla(240,100%,50%,0.7)}#cube .bottom{background:hsla(300,100%,50%,0.7)}#cube .front{-webkit-transform:translateZ(35vw);-moz-transform:translateZ(35vw);-o-transform:translateZ(35vw);transform:translateZ(35vw)}#cube .back{-webkit-transform:rotateX(-180deg) translateZ(35vw);-moz-transform:rotateX(-180deg) translateZ(35vw);-o-transform:rotateX(-180deg) translateZ(35vw);transform:rotateX(-180deg) translateZ(35vw)}#cube .right{-webkit-transform:rotateY(90deg) translateZ(35vw);-moz-transform:rotateY(90deg) translateZ(35vw);-o-transform:rotateY(90deg) translateZ(35vw);transform:rotateY(90deg) translateZ(35vw)}#cube .left{-webkit-transform:rotateY(-90deg) translateZ(35vw);-moz-transform:rotateY(-90deg) translateZ(35vw);-o-transform:rotateY(-90deg) translateZ(35vw);transform:rotateY(-90deg) translateZ(35vw)}#cube .top{-webkit-transform:rotateX(90deg) translateZ(35vw);-moz-transform:rotateX(90deg) translateZ(35vw);-o-transform:rotateX(90deg) translateZ(35vw);transform:rotateX(90deg) translateZ(35vw)}#cube .bottom{-webkit-transform:rotateX(-90deg) translateZ(35vw);-moz-transform:rotateX(-90deg) translateZ(35vw);-o-transform:rotateX(-90deg) translateZ(35vw);transform:rotateX(-90deg) translateZ(35vw)}";

function addcss(css){
  var head = document.getElementsByTagName('head')[0];
  var s = document.createElement('style');
  s.setAttribute('type', 'text/css');
  if (s.styleSheet) {   // IE
      s.styleSheet.cssText = css;
  } else {                // the world
      s.appendChild(document.createTextNode(css));
  }
  head.appendChild(s);
}

addcss(css);

var init = function() {

  var box = document.getElementById("cube"),
      showPanelButtons = document.querySelectorAll('#show-buttons button'),
      panelClassName = 'show-front',
      valueX = 0,
      valueY = 0,
      valueZ = 0,

      moveCube = function(event, o) {
        if(!o.inMotion) {
          switch(o.direction) {
            case "left" :
              if((valueY/90)%2){
                valueZ -= 90;
              };
              valueX -= 90;
              break;
            case "right" :
              if((valueY/90)%2){
                valueZ += 90;
              };
              valueX += 90;
              break;
            case "top" :
              valueY += 90;
              break;
            case "bottom" :
              valueY -= 90;
              break;
            default :
              break;
          }
            box.style.transform = "translateZ(-100px)rotateX("+valueY+"deg)rotateY("+valueX+"deg)rotateZ("+valueZ+"deg)";
            box.style.WebkitTransform = "translateZ(-100px)rotateX("+valueY+"deg)rotateY("+valueX+"deg)rotateZ("+valueZ+"deg)";
        }
    };
  _(".container").on("slide", moveCube);

};

window.addEventListener( 'DOMContentLoaded', init, false);
